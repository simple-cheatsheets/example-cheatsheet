echo "\\renewcommand{\\revisiondate}{`git log -1 --format=\"%ad\" --date=short $(pwd)`}" > revision.tex
echo "\\renewcommand{\\revision}{`git log -1 --format=\"%h\" $(pwd)`}" >> revision.tex
pdflatex --shell-escape document.tex
biber document
pdflatex --shell-escape document.tex
pdflatex --shell-escape document.tex
pdflatex --shell-escape document.tex
